<?php
/**
 * Created by: Stephan Hoeksema 2022
 * collegesdomp
 */
$router->define([
    '' => 'controllers/index.php',
    'students' => 'controllers/students.php',
    'teachers' => 'controllers/teachers.php',
    'courses' => 'controllers/courses.php'
]);