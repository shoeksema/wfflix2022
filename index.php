<?php
//Classes
require 'core/Router.php';


//Connection to the database
//$conn = require 'core/core.php';

//Router
$router = new Router();

//routes
require 'routes.php';

//fetch uri
$uri = trim($_SERVER['REQUEST_URI'], '/');


require $router->direct($uri);