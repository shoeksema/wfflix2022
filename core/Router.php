<?php

/**
 * Created by: Stephan Hoeksema 2022
 * wfflix
 */
class Router
{
    protected $routes = [];

    public function define($routes)
    {
        $this->routes = $routes;
    }

    public function direct($uri)
    {
        if (array_key_exists($uri, $this->routes)){
            die(var_dump($this->routes[$uri]));
            return $this->routes[$uri];
        }
        throw new Exception('No route defined');
    }
}